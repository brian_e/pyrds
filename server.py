#! /usr/bin/env python

import shelve
from signal import SIGINT, signal
from sys import argv
from SocketServer import BaseRequestHandler, TCPServer
from msgpack import packb, unpackb

data_store = shelve.open('data_store')


class RequestHandler(BaseRequestHandler):
    commands = (('DEL', 'delete'), ('GET', 'get'), ('SET', 'set'))

    def handle(self):
        print self.client_address
        reply = None
        try:
            cmd = unpackb(self.request.recv(1024))
            meth = {c: getattr(self, m) for c, m in self.commands}.get(cmd[0])
            if not meth:
                print "bad command: {}".format(cmd)
                return
            reply = meth(*cmd[1:])
        finally:
            self.request.sendall(packb(reply))

    def delete(self, *keys):
        count = 0
        for key in keys:
            if key in data_store:
                del data_store[key]
                data_store.sync()
                count += 1
        return count

    def get(self, key, *args):
        return data_store.get(key)

    def set(self, key, value, *args):
        data_store[key] = value
        data_store.sync()
        return 'OK'


if __name__ == "__main__":
    port = 6380 if len(argv) == 1 else int(argv[1])
    print "remote dictionary server listening on port", port
    server = TCPServer(('0.0.0.0', port), RequestHandler)

    def handle_interrupt(signum, frame):
        server.server_close()
        data_store.close()

    signal(SIGINT, handle_interrupt)
    server.serve_forever()
