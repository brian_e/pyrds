#! /usr/bin/env python

from contextlib import closing
from socket import AF_INET, SOCK_STREAM, socket
from sys import argv
from msgpack import packb, unpackb

command = argv[1:3]
if command == 'SET':
    command.append(' '.join(argv[3:]))
else:
    command.extend(argv[3:])
with closing(socket(AF_INET, SOCK_STREAM)) as sock:
    sock.connect(('localhost', 6380))
    sock.sendall(packb(command))
    received = sock.recv(1024)
print unpackb(received)
